package com.epsi.webservices.mapper;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import com.epsi.webservices.api.dto.ArticleDTO;
import com.epsi.webservices.model.Article;
import com.epsi.webservices.model.Commande;

@Mapper(componentModel = "spring")
public interface ArticleMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "commandeIds", source = "commandeList", qualifiedByName = "mapCommandeIds")
    ArticleDTO mapToDto(Article article);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "commandeList", source = "commandeIds", qualifiedByName = "mapToCommandes")
    Article mapToModel(ArticleDTO articleDto);

    @Named("mapCommandeIds")
    default List<Long> mapCommandeIds(List<Commande> commandeList) {
        if (commandeList != null) {
            return commandeList.stream()
                    .map(Commande::getId)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Named("mapToCommandes")
    default List<Commande> mapToCommandes(List<Long> commandeIds) {
        if (commandeIds != null) {
            return commandeIds.stream()
                    .map(id -> {
                        Commande commande = new Commande();
                        commande.setId(id);
                        return commande;
                    })
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }
}
