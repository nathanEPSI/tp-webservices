package com.epsi.webservices.mapper;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import com.epsi.webservices.api.dto.CommandeDTO;
import com.epsi.webservices.model.Article;
import com.epsi.webservices.model.Commande;

@Mapper(componentModel = "spring")
public interface CommandeMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "articleIds", source = "articleList", qualifiedByName = "mapArticleIds")
    CommandeDTO mapToDto(Commande commande);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "articleList", source = "articleIds", qualifiedByName = "mapToArticles")
    Commande mapToModel(CommandeDTO commandeDTO);

    @Named("mapArticleIds")
    default List<Long> mapArticleIds(List<Article> articleList) {
        if (articleList != null) {
            return articleList.stream()
                    .map(Article::getId)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Named("mapToArticles")
    default List<Article> mapToArticles(List<Long> articleIds) {
        if (articleIds != null) {
            return articleIds.stream()
                    .map(id -> {
                        Article article = new Article();
                        article.setId(id);
                        return article;
                    })
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }
}
