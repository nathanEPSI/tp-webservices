package com.epsi.webservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epsi.webservices.model.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> { 
}
