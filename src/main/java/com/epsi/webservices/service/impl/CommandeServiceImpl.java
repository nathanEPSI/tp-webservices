package com.epsi.webservices.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.epsi.webservices.exception.UnknownRessourceException;
import com.epsi.webservices.model.Commande;
import com.epsi.webservices.repository.CommandeRepository;
import com.epsi.webservices.service.CommandeService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CommandeServiceImpl implements CommandeService {

    private final CommandeRepository commandeRepository;

    @Override
    public List<Commande> getAllCommandes() {
        return commandeRepository.findAll();
    }

    @Override
    public Commande getCommandeById(Long id) {
        return commandeRepository.findById(id)
                .orElseThrow(() -> new UnknownRessourceException("No Commande with this ID"));
    }

    @Override
    public Commande createCommande(Commande commande) {
        return commandeRepository.save(commande);
    }

    @Override
    public Commande updateCommande(Commande commande) {
        return commandeRepository.save(commande);
    }

    @Override
    public void deleteCommandeById(Long id) {
        Commande commandeToDelete = getCommandeById(id);
        commandeRepository.delete(commandeToDelete);
    }
}

