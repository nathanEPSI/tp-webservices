package com.epsi.webservices.service;

import java.util.List;

import com.epsi.webservices.model.Commande;

public interface CommandeService {
    List<Commande> getAllCommandes();

    Commande getCommandeById(Long id);

    Commande updateCommande(Commande commande);

    Commande createCommande(Commande commande);

    void deleteCommandeById(Long id);
}
