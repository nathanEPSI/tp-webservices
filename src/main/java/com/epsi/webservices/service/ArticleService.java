package com.epsi.webservices.service;

import java.util.List;

import com.epsi.webservices.model.Article;

public interface ArticleService {
    List<Article> getAllArticles();

    Article getArticleById(Long id);

    Article updateArticle(Article article);

    Article createArticle(Article article);

    void deleteArticleById(Long id);
}

