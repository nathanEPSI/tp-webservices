package com.epsi.webservices.exception;

public class UnknownRessourceException extends RuntimeException {
    public UnknownRessourceException(String message){
        super(message);
    }
}
