package com.epsi.webservices.model;

import java.util.List;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "article")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, name = "designation")
    private String designation;

    @Column(name = "quantite")
    private int quantite;

    @Column(name = "prix")
    private float prix;

    @ManyToMany(mappedBy = "articleList")
    private List<Commande> commandeList;    
}
