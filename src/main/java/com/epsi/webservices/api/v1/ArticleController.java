package com.epsi.webservices.api.v1;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.epsi.webservices.api.dto.ArticleDTO;
import com.epsi.webservices.exception.UnknownRessourceException;
import com.epsi.webservices.mapper.ArticleMapper;
import com.epsi.webservices.model.Article;
import com.epsi.webservices.service.ArticleService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "api/v1/articles", produces = MediaType.APPLICATION_JSON_VALUE)
public class ArticleController {

    private final ArticleService articleService;
    private final ArticleMapper articleMapper;

    public ArticleController(ArticleService articleService, ArticleMapper articleMapper) {
        this.articleService = articleService;
        this.articleMapper = articleMapper;
    }

    @GetMapping
    public ResponseEntity<List<ArticleDTO>> getAllArticles() {
        List<ArticleDTO> articles = articleService.getAllArticles().stream()
                .map(articleMapper::mapToDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(articles);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ArticleDTO> getArticleById(@PathVariable Long id) {
        try {
            ArticleDTO articleDto = articleMapper.mapToDto(articleService.getArticleById(id));
            return ResponseEntity.ok(articleDto);
        } catch (UnknownRessourceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ArticleDTO> updateArticle(@RequestBody ArticleDTO articleDto, @PathVariable Long id) {
        try {
            articleDto.setId(id);
            Article article = articleMapper.mapToModel(articleDto);
            Article updatedArticle = articleService.updateArticle(article);
            ArticleDTO updatedArticleDto = articleMapper.mapToDto(updatedArticle);
            return ResponseEntity.ok(updatedArticleDto);
        } catch (UnknownRessourceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<ArticleDTO> createArticle(@RequestBody ArticleDTO articleDto) {
        Article article = articleMapper.mapToModel(articleDto);
        Article createdArticle = articleService.createArticle(article);
        ArticleDTO createdArticleDto = articleMapper.mapToDto(createdArticle);
        URI location = URI.create("/api/v1/articles/" + createdArticleDto.getId());
        return ResponseEntity.created(location).body(createdArticleDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteArticleById(@PathVariable Long id) {
        try {
            articleService.deleteArticleById(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownRessourceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

