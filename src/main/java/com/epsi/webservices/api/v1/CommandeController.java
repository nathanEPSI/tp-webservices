package com.epsi.webservices.api.v1;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.epsi.webservices.api.dto.CommandeDTO;
import com.epsi.webservices.exception.UnknownRessourceException;
import com.epsi.webservices.mapper.CommandeMapper;
import com.epsi.webservices.model.Article;
import com.epsi.webservices.model.Commande;
import com.epsi.webservices.service.ArticleService;
import com.epsi.webservices.service.CommandeService;


import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(path = "api/v1/commandes", produces = MediaType.APPLICATION_JSON_VALUE)
public class CommandeController {

    private final CommandeService commandeService;
    private final CommandeMapper commandeMapper;
    private final ArticleService articleService;

    public CommandeController(CommandeService commandeService, CommandeMapper commandeMapper, ArticleService articleService) {
        this.commandeService = commandeService;
        this.commandeMapper = commandeMapper;
        this.articleService = articleService;
    }

    @GetMapping
    public ResponseEntity<List<CommandeDTO>> getAllCommandes() {
        List<CommandeDTO> commandes = commandeService.getAllCommandes().stream()
                .map(commandeMapper::mapToDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(commandes);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CommandeDTO> getCommandeById(@PathVariable Long id) {
        try {
            CommandeDTO commandeDto = commandeMapper.mapToDto(commandeService.getCommandeById(id));
            return ResponseEntity.ok(commandeDto);
        } catch (UnknownRessourceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<CommandeDTO> updateCommande(@RequestBody CommandeDTO commandeDto, @PathVariable Long id) {
        try {
            commandeDto.setId(id);
            Commande commande = commandeMapper.mapToModel(commandeDto);
            Commande updatedCommande = commandeService.updateCommande(commande);
            CommandeDTO updatedCommandeDto = commandeMapper.mapToDto(updatedCommande);
            return ResponseEntity.ok(updatedCommandeDto);
        } catch (UnknownRessourceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<CommandeDTO> createCommande(@RequestBody CommandeDTO commandeDto) {
        Commande commande = commandeMapper.mapToModel(commandeDto);
        Commande createdCommande = commandeService.createCommande(commande);
    
        // Réduire la quantité des articles en fonction de la liste articleIds
        List<Long> articleIds = commandeDto.getArticleIds();
        reduceArticleQuantities(articleIds);
    
        CommandeDTO createdCommandeDto = commandeMapper.mapToDto(createdCommande);
        URI location = URI.create("/api/v1/commandes/" + createdCommandeDto.getId());
        return ResponseEntity.created(location).body(createdCommandeDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCommandeById(@PathVariable Long id) {
        try {
            commandeService.deleteCommandeById(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownRessourceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    private void reduceArticleQuantities(List<Long> articleIds) {
        Map<Long, Long> articleCountMap = countOccurrences(articleIds);
        for (Long articleId : articleCountMap.keySet()) {
            Article article = articleService.getArticleById(articleId);
            Long count = articleCountMap.get(articleId);
            if (count != null) {
                article.setQuantite(article.getQuantite() - count.intValue());
                articleService.updateArticle(article);
            }
        }
    }
    
    private Map<Long, Long> countOccurrences(List<Long> list) {
        Map<Long, Long> countMap = new HashMap<>();
        for (Long item : list) {
            countMap.put(item, countMap.getOrDefault(item, 0L) + 1);
        }
        return countMap;
    }
}
