package com.epsi.webservices.mapper;

import com.epsi.webservices.api.dto.CommandeDTO;
import com.epsi.webservices.model.Commande;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-05-22T15:58:51+0200",
    comments = "version: 1.5.5.Final, compiler: Eclipse JDT (IDE) 3.34.0.v20230413-0857, environment: Java 17.0.7 (Eclipse Adoptium)"
)
@Component
public class CommandeMapperImpl implements CommandeMapper {

    @Override
    public CommandeDTO mapToDto(Commande commande) {
        if ( commande == null ) {
            return null;
        }

        CommandeDTO commandeDTO = new CommandeDTO();

        commandeDTO.setId( commande.getId() );
        commandeDTO.setArticleIds( mapArticleIds( commande.getArticleList() ) );

        return commandeDTO;
    }

    @Override
    public Commande mapToModel(CommandeDTO commandeDTO) {
        if ( commandeDTO == null ) {
            return null;
        }

        Commande commande = new Commande();

        commande.setId( commandeDTO.getId() );
        commande.setArticleList( mapToArticles( commandeDTO.getArticleIds() ) );

        return commande;
    }
}
