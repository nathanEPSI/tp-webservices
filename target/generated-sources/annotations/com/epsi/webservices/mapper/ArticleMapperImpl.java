package com.epsi.webservices.mapper;

import com.epsi.webservices.api.dto.ArticleDTO;
import com.epsi.webservices.model.Article;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-05-22T15:58:51+0200",
    comments = "version: 1.5.5.Final, compiler: Eclipse JDT (IDE) 3.34.0.v20230413-0857, environment: Java 17.0.7 (Eclipse Adoptium)"
)
@Component
public class ArticleMapperImpl implements ArticleMapper {

    @Override
    public ArticleDTO mapToDto(Article article) {
        if ( article == null ) {
            return null;
        }

        ArticleDTO articleDTO = new ArticleDTO();

        articleDTO.setId( article.getId() );
        articleDTO.setCommandeIds( mapCommandeIds( article.getCommandeList() ) );
        articleDTO.setDesignation( article.getDesignation() );
        articleDTO.setPrix( article.getPrix() );
        articleDTO.setQuantite( article.getQuantite() );

        return articleDTO;
    }

    @Override
    public Article mapToModel(ArticleDTO articleDto) {
        if ( articleDto == null ) {
            return null;
        }

        Article article = new Article();

        article.setId( articleDto.getId() );
        article.setCommandeList( mapToCommandes( articleDto.getCommandeIds() ) );
        article.setDesignation( articleDto.getDesignation() );
        article.setPrix( articleDto.getPrix() );
        if ( articleDto.getQuantite() != null ) {
            article.setQuantite( articleDto.getQuantite() );
        }

        return article;
    }
}
